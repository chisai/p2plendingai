#!/usr/bin/env python

from __future__ import absolute_import, division, print_function

import tensorflow as tf
from tensorflow import keras, layers
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense

import pandas as pd
import numpy as np
from random import randint
import mysql.connector
import sys
import os

workingpath = os.getcwd()
workingpath = workingpath.replace("public", "resources/scripts")

who1 = sys.argv[1]
who = int(float(who1[1:]))
risk = int(float(who1[:1]))

from settings import settings

mydb = mysql.connector.connect(
host=settings("DB_HOST"),
user=settings("DB_USERNAME"),
passwd=settings("DB_PASSWORD"),
port=settings("DB_PORT"),
database=settings("DB_DATABASE")
)
mycursor = mydb.cursor()

def fetchdata():
    sql = "SELECT * FROM secondmarketitem ORDER BY id DESC LIMIT {}".format(who) 
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    for rsl in myresult:    
        sql1 = "SELECT * FROM predictions WHERE auction_id={}".format(rsl[0])
        mycursor.execute(sql1)
        newresult = mycursor.fetchone()
        if not newresult:
            arr = (rsl[0], rsl[13], rsl[22])
            predict(arr)  
    return True

def storedata(id, rating):
    sql = "INSERT INTO predictions (auction_id, rating) VALUES (%s, %s) ON DUPLICATE KEY UPDATE rating=%s" 
    val = (id, rating, rating)
    mycursor.execute(sql, val)
    mydb.commit()
    return mycursor.rowcount

class_name = [      #Labels rating from high to low
    'AA',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'HR'
]

def predict(array):
    list_predict = np.array([[array[1], array[2]]])
    predictions = model.predict(list_predict)
    test = np.argmax(predictions[0])
    test1 = test.item()
    t13 = storedata(array[0], test1)

def get_csv_data (filename):
    csv = pd.read_csv(filename)
    data = csv.copy()
    data = np.array(data)
    return data

model = tf.keras.Sequential(
    [
        tf.keras.layers.Dense(256,activation='relu',input_shape=(2,)),
        tf.keras.layers.Dense(2,activation='softmax')
    ]
)

model.compile(
    optimizer='adam',
    loss="sparse_categorical_crossentropy",
    metrics=['accuracy']
)
datapath = workingpath + "/data.csv"
data = get_csv_data(datapath)


csvfile = '/labels{}.csv'.format(risk)
labelpath = workingpath + csvfile
labels = get_csv_data(labelpath)

model.fit(data, labels, epochs=10, batch_size=512, verbose=0)
pred = ["Low Risk", "High Risk"]

t12 = fetchdata()
temp1 = 0