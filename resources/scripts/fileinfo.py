#!/usr/bin/env python

import os
import mysql.connector
import hashlib
import time

workingpath = os.getcwd()
workingpath = workingpath.replace("public", "resources/scripts/")
workingpath = workingpath + '/LoanData.zip'
url1 = "www.bondora.com/marketing/media/LoanData.zip"
info = os.stat(workingpath)
filesize = info.st_size
hash1 = str(hashlib.md5(workingpath.encode('utf-8')))[-15:-1]
timestamp = int(time.time())

def insertDB(arr):
    sql = "INSERT INTO datasetlog (hash, size, url, status, created_at) VALUES (%s, %s, %s, %s, %s)"
    mycursor.execute(sql, arr)
    mydb.commit()
    return

def updateDB(status, id):
    sql = "INSERT INTO datasetlog (id, hash, url, status) VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE status=%s"
    arr = (id, hash1, url1, status, status)
    mycursor.execute(sql, arr)
    mydb.commit()
    return

from settings import settings

mydb = mysql.connector.connect(
host=settings("DB_HOST"),
user=settings("DB_USERNAME"),
passwd=settings("DB_PASSWORD"),
port=settings("DB_PORT"),
database=settings("DB_DATABASE")
)
mycursor = mydb.cursor()

sql = "SELECT * FROM datasetlog ORDER BY id DESC LIMIT 1"
mycursor.execute(sql)
myresult = mycursor.fetchone()

if not myresult:
    val = (hash1, filesize, url1, 'OK', timestamp)
    insertDB(val)

if myresult:
    if myresult[4] == filesize: 
        if (timestamp - myresult[5]) < 86400:
            timeleft = int(((86400 - (timestamp - myresult[5])) / 86400) * 24)
            val = ('Already updated, try updating once a day! Try again in {} hour(s)').format(timeleft)
            updateDB(val, myresult[0])
            print("Dataset not updated! Try again in a day")
        else:
            val = (hash1, filesize, url1, 'OK', timestamp)
            insertDB(val)
            print("Dataset updated")
    elif myresult[4] > filesize:
        val = (hash1, filesize, url1, 'DATA CORRUPTION', timestamp)
        insertDB(val)
        print("Dataset not updated!")
    else:
        val = (hash1, filesize, url1, 'OK', timestamp)
        insertDB(val)
        print("Dataset updated")