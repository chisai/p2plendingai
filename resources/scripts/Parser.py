import pandas as pd
import numpy as np
import os

if os.path.isfile('labels0.csv'):
    lblFile = "labels1.csv"
    rating = 7
else:
    lblFile = "labels0.csv"
    rating = 4

labels = {
    'AA': 0,
    'A': 1,
    'B': 2,
    'C': 3,
    'D': 4,
    'E': 5,
    'F': 6,
    'HR': 7
}

allowedCollumns = [
    # 'Age',
    # 'Gender',
    # 'Amount',
    'Interest',
    'LoanDuration',
    #'MonthlyPayment'
]

filename = 'LoanData.csv'
data = pd.read_csv(filename, low_memory=False)

d = pd.DataFrame(data)
d1 = pd.DataFrame([d.pop(x) for x in allowedCollumns]).T
rating_collumn = pd.DataFrame(d.pop('Rating')).T


for row in rating_collumn:
    try:
        r = labels[rating_collumn[row].Rating]
        if r < rating:
            rating_collumn[row].Rating = 0
        else:
            rating_collumn[row].Rating = 1
        pass
    except:
        rating_collumn[row].Rating = 1 #7
        pass
        
dataset = d1.to_csv(path_or_buf='data.csv', index=False)
dd = pd.DataFrame(rating_collumn).T
labels = dd.to_csv(path_or_buf=lblFile,index=False)