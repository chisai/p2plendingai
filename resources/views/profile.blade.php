@extends('layouts.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card-header">
               <strong style="font-size: 1.5rem;">{{ $user->name }}'s Profile</strong><span class="pull-right"><a href="{{ route('deposit') }}">Deposit now!</a></span>
            </div>
            <div class="card-body">
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                <form method="post" action="{{ route('users.update', Auth::user()) }}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}

                <table>
                    <tr>
                        <td>ID: </td><td>{{ $user->id }}</td>
                    </tr>
                    <tr>
                        <td>Name: </td><td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>Email: </td><td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>Password: </td><td><input type="password" name="password"></td>
                    </tr>
                    <tr>
                        <td>Confirm password: </td><td><input type="password" name="password_confirm"></td>
                    </tr>
                    <tr>
                        <td><button style="margin-top: 3px" type="submit">Update</button></td>
                    </tr>
               
                </table>      
            </form>
            </div>
            <div class="card-header">
                <strong style="font-size: 1.5rem;">Transaction History</strong>    
            </div>
            <div class="card-body">
                <div class="text-center">
                Balance:<strong style="font-size: 1.1rem"> €{{ number_format($balance,2) }}</strong>
                </div>
                <hr>
                
                <div class="table-responsive text-center">
                <table style="width: 100%" class="text-center">
                    <tr>
                        <th>Transaction ID:</th><th>Transaction Date/Time</th><th>Transaction Type:</th><th>Amount:</th>
                @foreach ($transactions as $transaction)
                <tr>
                    @if ($transaction->transaction_type == "deposit")
                        <?php $color = "green"; ?>
                    @elseif ($transaction->transaction_type == "investment")
                        <?php $color = "orange"; ?>
                    @else
                        <?php $color = "red"; ?>
                    @endif
                    
                <td>{{ $transaction->transaction_id }}</td><td>{{ $transaction->created_at }}</td><td style="color: {{ $color }};"> {{ $transaction->transaction_type }}</td> <td>{{ number_format($transaction->amount,2) }}</td>
                </tr>
                @endforeach
            </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
