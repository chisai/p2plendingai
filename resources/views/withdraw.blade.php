@extends('layouts.layout')

@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="formBox">
            @if (isset($error)) 
            <div class="error"><h3 style="color:red"><strong>{{ $error }}</strong></h3></div>
            @endif
            <form action="{{ route('withdrawal') }}" method="post" id="checkout-form">
                @csrf
                @method('post')
                <div class="row">
                    <div class="col-sm-12">
                        <h1><center>Withdrawal</center><span class="pull-right balance" style="font-size: 1.2rem;">Balance: €{{ number_format($balance,2) }}</span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <label for="account-name inputText">Bank Account Holder Name</label>
                            <input type="text" name="account" id="account-name" class="form-control input" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="inputBox">
                            <label for="iban inputText">IBAN</label>
                            <input type="text" name="iban" id="iban" class="form-control input" required>
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                                <label for="swift inputText">SWIFTBIC</label>
                                <input type="text" name="swift" id="swift" class="form-control input" required>
                                <label for="amount inputText">Amount</label>
                                <input type="number" min="0" step="0.01" id="amount" name="amount" class="form-control input" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center" style="color: red">
                    The time it takes for the payment to complete depends on your bank, it can take up to 30 days!
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-black1">Withdraw now!</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
