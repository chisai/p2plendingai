@extends('layouts.layout')

@section('content')
<div class="container-fluid">
    <div class="container">
        <input id="key" type="hidden" value="{{ Config::get('services.stripe.key') }}">
        <div class="formBox">
            <form action="{{ route('checkout') }}" method="post" id="checkout-form">
                @csrf
                @method('post')
                <div class="row">
                    <div class="col-sm-12">
                        <h1><center>Deposit</center><span class="pull-right balance" style="font-size: 1.2rem;">Balance: €{{ number_format($balance,2) }}</span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <label for="card-name inputText">Card Holder Name</label>
                            <input type="text" id="card-name" class="form-control input" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="inputBox">
                            <label for="card-number inputText">Card Number</label>
                            <input type="number" id="card-number" class="form-control input" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <label for="card-expiry-month inputText">Card Exp. Month</label><br>
                            <select id="card-expiry-month" class="form-control input" required>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <label for="card-expiry-year inputText ">Card Exp. Year</label>
                            <select id="card-expiry-year" class="form-control input" required>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                                <label for="card-cvc inputText">Card CVC</label>
                                <input type="text" id="card-cvc" class="form-control input" required>
                                <label for="amount inputText">Amount</label>
                                <input type="number" min="0" step="0.01" id="amount" name="amount" class="form-control input" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-black1">Deposit now!</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://js.stripe.com/v2/"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ URL::to('/js/checkout.js') }}"></script>
@endsection