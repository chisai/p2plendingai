@extends('layouts.layout')

@section('style')
<style>
#risk {
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    opacity: 0;
    z-index: 0;
}

.choice:hover {
    
    -webkit-transition: all 0.3s ease-in;
    -moz-transition: all 0.3s ease-in;
    -o-transition: all 0.3s ease-in;
    transition: all 0.3s ease-in;
    z-index: 1;
    height: 250px;
}

.choice:hover #risk {
    -webkit-transition: all 0.5s ease-in;
    -moz-transition: all 0.5s ease-in;
    -o-transition: all 0.5s ease-in;
    transition: all 0.5s ease-in;
    opacity: 1;
    z-index: 10;
}
.choice {
    position: relative;
    height: 210px;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    z-index: 0;
}

</style>
@endsection

@section('content')
<?php
putenv('LANG=en_US.UTF-8');
shell_exec('python3 ' . Session::get('workingDir') . '/test-ai.py ' . $risk . $amount . ' 2>&1');
?>
<div class="container text-center">
    <div class="card">
            <div class="card-header">
               <strong style="font-size: 1.5rem;">Investment Choices</strong>
            </div>
            <div class="card-body">
                @if ($risk == 0)
                    You have chosen: <strong>Low Risk</strong><br>
                @else
                    You have chosen: <strong>High Risk</strong><br>
                @endif
                Green is a recommended transaction<br>
                Red is unrecommended<br><br>
                <div class="row">
            @foreach ($items as $item)
            <?php 
                $outputs = DB::table('predictions')->where('auction_id', $item->id)->limit(1)->get();
            ?>
                @foreach ($outputs as $output)
               <?php
                $borderColor = "#9A0202";
                if ($output->rating == 0) {
                    $borderColor = "green";
                } 
               ?>         
                <div style="box-shadow: 1px 1px 1px 1px #CCE5FB;border: 1px solid #CCE5FB; margin-left: auto; margin-top: 12px; margin-bottom: 12px; margin-right: auto; border-radius: 4px;" onclick="myFunction({{ $item->id }})" class="col-sm-6 col-md-5 col-lg-5 text-center card-header choice">    
                @endforeach
                <form action="{{ route('made.choice', $item->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}
                    <table>
                     @foreach ($keys as $k)
                    <tr>
                        <td>{{ $k }}:</td><td name={{ $k }}>{{ $item->$k }}</td>
                    </tr>
                    @endforeach 
                    </table>
                        <button style="color: white; margin-top: 3px; background: {{ $borderColor }}" type="submit">Choose me!</button>
                    <div id="risk">
                    @if ($output->rating == 0) 
                        <br>{{ 'YES! INVEST!' }}
                    @else
                        <br>{{ 'WARNING: HIGH RISK!' }}
                    @endif
                    </div>    
                    </div>
                </form>
                
                @endforeach     
            </div>
            <a href="{{ route('home') }}"><button class="btn btn-info">Back</button></a>
            </div>
        </div>
        </div> 
    </div>
</div>
@endsection

@section('scripts')
<script>
function myFunction(id) {
    var temp = String(window.location);
    var temp1 = temp.search('items');
    var temp2 = temp.substr(0, temp1) + "made/choice/";
   window.location.href = temp2 + id;
}
</script>    
@endsection
