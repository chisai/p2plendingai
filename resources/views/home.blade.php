@extends('layouts.layout')

@section('style')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }
    
    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }
    
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #2D9883;
        -webkit-transition: .4s;
        transition: .4s;
    }
    
    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: #fff;
        -webkit-transition: .4s;
        transition: .4s;
    }
    
    input:checked + .slider {
        background-color: #ff0000;
    }
    
    input:focus + .slider {
        box-shadow: 0 0 1px #ff0000;
    }
    
    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
    
    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }
    
    .slider.round:before {
        border-radius: 50%;
    }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard 
                    <li class="nav-item dropdown pull-right">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Balance: €{{ number_format($balance,2) }}<span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href={{ route('deposit') }}>
                                {{ __('Deposit now') }}
                            </a>
                            <a class="dropdown-item" href={{ route('pre.withdraw') }}>
                                {{ __('Withdraw now') }}
                            </a>
                        </div>
                    </li>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                      </div>
                    @endif
                    <?php Session::forget('status'); Session::forget('error'); ?>
                    Welcome {{ Auth::user()->name }}
                    <br>  
                    You are logged in!<br>
                    Let's invest some money, to make money
                    <form action={{ route('getData') }} method="get">
                        <br><br>Low Risk
                        <label style="top: -8px" class="switch">
                        <input name="risk" type="checkbox">
                        <span class="slider round"></span>
                        </label>
                        High Risk
                    
                        <br><br>
                        Amount of results:<br><br>
                        <table style="width: 150px;">
                            <tr>    
                                <td><input type="radio" name="amount" value="4" checked></td>
                                <td><input type="radio" name="amount" value="8"></td>
                                <td><input type="radio" name="amount" value="12"></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>8</td>
                                <td>12</td>
                            </tr>
                        </table><br><br>
                        <input type="submit" value="Show my Investment options">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection