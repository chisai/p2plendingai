@extends('layouts.app')

@section('content')

<a href="{{ route('home') }}"><img src="/image/financechisai.png" class="img-fluid" alt="Responsive image"><a>

<div class="jumbotron" id="Peer2Peer">
    <h1 class="display-4">
        <center>Peer 2 Peer Lending</center>
    </h1>
        <center><img src="image/businessmodel.jpg" alt="lending" style="width: 50%;"></center>
    <hr class="my-4">
    <p class="lead"><center>Slowly Europe is beginning to learn about peer-to-peer (p2p) loans.<br>
        Also in the Netherlands there are already several platforms that make it possible to lend your money to an equal, to get a nice merit from it.
       <br> But the search for a nice pear is not so easy.
       <br> Peer-to-peer literally means equal to equal, so the person who provides the loan is equal to the person who takes out the loan.
        <br>Both are (often) private individuals.
        <br><br>
        This is different from crowdfunding, for example, where the money seeker is an entrepreneur in almost all cases and the investor does not have to.
        <br>For p2p projects, you usually borrow money for private business, such as financing a new bathroom or renovating a home.</center>
        </p>
    <hr class="my-4">
</div>

<div class="jumbotron jumbotron-fluid" id="invest">
        <div class="container">
          <h1 class="display-4">Start your investment now!</h1>
          <p class="lead">Your investments are always secured as all loans listed on the platform come with 100% buy-back guarantee by the originator. When a loan goes into default the originator will repay your invested principal.
             <br><br>Don't miss out on good investment opportunities. Use our auto invest feature and your funds will always be fully invested according to your individual preferences.
             <br><br>Some people believe the only way to invest money is to buy stocks and bonds.
             <br><br>However, this is not the case nowadays. In fact, pension funds and other sophisticated financial institutions understand that portfolio diversification is the key to long-term success. By expanding the range of asset classes they invest in, investors can increase the odds of generating attractive risk-adjusted returns.
             <br><br>Of course, there’s much more to it than that. To follow in the footsteps of the professionals who manage the assets of millions of individuals and organizations around the world, it is important to have an idea of what matters most when it comes to making your money work harder for you.
          </p>
          <img src="image/banking.JPG"  alt="invest" class="img img-fluid " style="width: 70%; margin:auto; display:block;">
        </div>
      </div>

<div class="row" id="App">
    <div class="col-12 col-md-8" style=" margin:150px auto; display:block;">
        <img src="image/available-on-the-app-store.svg" alt="apple" style="width: 35%;" class="img-fluid">
        <img src="image/get-it-on-google-play.svg" alt="google" style="width: 35%; float:right; transform:translate(-30%, 0%);"class="img-fluid"></div>
        <div class="row" style="margin:0px auto; display:block;">
        <div class="col"><img src="image/iphonechisai.png" alt="google"  class="img-fluid" style="width: 60%; transform: translate(30%, 0%);">
    </div>
</div>
</div>


@endsection