@extends('layouts.layout')

@section('content')
<div class="container">
    <div class="card">
            <div class="card-header text-center">
               <strong style="font-size: 1.5rem;">{{ $data->LoanId }}</strong>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <button style="margin-top: 3px" class="btn btn-info" onclick="goBack()">Back</button><br>
                </div>    
                <div class="text-center">
                {{-- You have chosen: <strong>{{  $data->LoanId }}</strong>,<br>
                Applied by: <strong>{{ $data->UserName }}</strong> from <strong>{{ $data->Country }}</strong>,<br>
                <strong>{{ $data->UserName }}</strong> is looking for an amount of &euro;<strong>{{ $data->Amount }}</strong><br>
                <br>
                Interest Rate: <strong>{{ $data->Interest }}%</strong><br>
                Credit Rating: <strong>{{  $data->Rating }}</strong> (AA is the highest rating)<br>
                Credit Score: <strong>{{ $data->CreditScore }}</strong> (0 is the lowest)<br> --}}
                <br>
                <table align="center">
                @foreach ($fillable as $key)
                    <tr>
                        <td>{{ $key }}:</td><td>{{ $data->$key }}</td>
                    </tr>
                @endforeach
                </table><br>
                <form action="{{ route('place.bid', $data->AuctionId) }}">
                    {{ csrf_field() }}
                    {{ method_field('patch') }}
                    <button style="margin-top: 3px" type="submit">Proceed</button>
                </form>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
function goBack() {    
    window.history.back();
}
</script>    
@endsection
