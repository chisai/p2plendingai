import pandas as pd
import numpy as np

labels = {
    'AA': 0,
    'A': 1,
    'B': 2,
    'C': 3,
    'D': 4,
    'E': 5,
    'F': 6,
    'HR': 7
}

allowedCollumns = [
    # 'Age',
    # 'Gender',
    # 'Amount',
    'Interest',
    'LoanDuration',
    #'MonthlyPayment'
]

filename = 'LoanData.csv'
print('Loading Data: ' + filename)
data = pd.read_csv(filename)

d = pd.DataFrame(data)
d1 = pd.DataFrame([d.pop(x) for x in allowedCollumns]).T
rating_collumn = pd.DataFrame(d.pop('Rating')).T

print('Parsing Rating to integer')

for row in rating_collumn:
    try:
        r = labels[rating_collumn[row].Rating]
        if r < 7:
            rating_collumn[row].Rating = 0
        else:
            rating_collumn[row].Rating = 1
        pass
    except:
        rating_collumn[row].Rating = 1 #7
        pass
        
print('Writing data to data.csv')
dataset = d1.to_csv(path_or_buf='data.csv', index=False)
dd =pd.DataFrame(rating_collumn).T
print('Writing labels to labels.csv')
labels = dd.to_csv(path_or_buf='labels1.csv',index=False)