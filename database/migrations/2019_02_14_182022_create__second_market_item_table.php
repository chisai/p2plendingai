<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondMarketItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondmarketitem', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->float('Amount');
            $table->string('LoanId')->unique();
            $table->string('AuctionId');
            $table->string('LoanPartId');
            $table->string('AuctionName')->nullable()->default(null);
            $table->integer('AuctionNumber');
            $table->integer('AuctionBidNumber');
            $table->string('Country');
            $table->double('CreditScore');
            $table->string('Rating');
            $table->float('Interest');
            $table->string('UserName');
            $table->date('LoanStatusActiveFrom');
            $table->integer('Gender');
            $table->date('DateOfBirth');
            $table->date('SignedDate');
            $table->integer('NextPaymentNr');
            $table->date('NextPaymentDate');
            $table->float('NextPaymentSum');
            $table->integer('NrOfScheduledPayments');
            $table->float('OutstandingPayments');
            $table->date('ListedOnDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondmarketitem');
    }
}
