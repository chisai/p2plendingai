<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('welcome');
//});



Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function() {

    Route::get('/deposit', ['uses' => 'UserController@deposit', 'as' => 'deposit']);

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/profile', 'UserController@profile');
    
    Route::post('/profile', 'UserController@updateAvatar');

    Route::patch('users/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@editUser']);

    Route::post('/deposit/working' , ['uses' => 'UserController@postDeposit', 'as' => 'checkout']);

    Route::get('/authorize','bondora@authorizeUser')->name('authorize');
    Route::get('/authme','bondora@authcheck');
    Route::get('/token','bondora@token');
    Route::get('/items','bondora@items')->name('getData');

    Route::get('/choices', ['uses' => 'SecondMarketController@storeItems', 'as' => 'choices']);

    Route::get('/made/choice/{id}', ['uses' => 'SecondMarketController@madeChoice', 'as' => 'made.choice']);

    Route::get('/place/bid/{id}', ['uses' => 'bondora@placeBid', 'as' => 'place.bid']);

    Route::get('/withdraw', ['uses' => 'UserController@withdraw', 'as' => 'pre.withdraw']);

    Route::post('/withdraw/working', ['uses' => 'UserController@postWithdraw', 'as' => 'withdrawal']);

});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/loans','bondora@loans');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcometest');
});
