from __future__ import absolute_import, division, print_function

import tensorflow as tf
from tensorflow import keras, layers
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#import seaborn as sns
from random import randint


filename = ['./output.csv']

record_defaults = (
    tf.constant([-1], dtype=tf.int32),              # tf.int32,     #Age
    #tf.constant(['UNKNOWN'], dtype=tf.string),      # tf.string,    #Birthdate
    tf.constant([2], dtype=tf.int32),               # tf.int32,     #Gender
    #tf.constant(['UNKNOWN'], dtype=tf.string),      # tf.string,    #Country
    tf.constant([0.0], dtype=tf.float32),           # tf.float32,   #Amount
    tf.constant([0.0], dtype=tf.float32),           # tf.float32,   #Interest
    tf.constant([0], dtype=tf.int32),               # tf.int32,     #Duration
    tf.constant([0.0], dtype=tf.float32),           # tf.float32,   #Monthly payment amount
    #tf.constant(['UNKNOWN'], dtype=tf.string),      # tf.string,    #County
    #tf.constant(['UNKNOWN'], dtype=tf.string),      # tf.string,    #City
    #tf.constant(['HR'], dtype=tf.string),           # tf.string     #Rating
)

class_name = [      #Labels rating from high to low
    'AA',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'HR'
]

def plot_history(history):
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history .epoch

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Mean Abs Error[mpg?]')
    plt.plot(hist['epoch'], hist['mean_absolute_error'], label="Train Error")
    plt.plot(hist['epoch'], hist['val_mean_absolute_error'], label="Val Error")
    plt.legend()
    plt.ylim([0,5])
    
    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Mean square Error [$MPG^2]')
    plt.plot(hist['epoch'],hist['mean_squared_error'], label="Train Error")
    plt.plot(hist['epoch'],hist['val_mean_squared_error'], label="val Error")
    plt.legend()
    plt.ylim([0,20])

def get_csv_data (filename):
    csv = pd.read_csv(filename)
    data = csv.copy()
    data = np.array(data)
    return data
    

# select_cols = []
# select_cols_count = 0

data = tf.data.experimental.CsvDataset(filename,record_defaults,header=True,select_cols=[0,2,4,5,6,7])
data1 = tf.data.experimental.CsvDataset(filename, [tf.constant([7], dtype=tf.int32)],header=True,select_cols=[10])
should_drop = tf.placeholder(tf.bool)

model = tf.keras.Sequential(
    [
        tf.keras.layers.Dense(512, activation='relu', input_shape=(2,)),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dense(7, activation='softmax')
    ]
)

model.compile(
    optimizer='Adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)
model.summary()


data = get_csv_data('data.csv')
labels = get_csv_data('labels.csv')
list_predict = [[43,0,635.0000,36.53,48,27.4600]]
list_predict = np.array(list_predict)

model.fit(data, labels, epochs=30, batch_size=256) #7089
# print(history)
#plot_history(history)
test_loss, test_acc = model.evaluate(data, labels)
print('Test accuracy:', round(test_acc * 100,2), "%")
predictions = model.predict(list_predict)
test = np.argmax(predictions[0])
print("Prediction of Credit Rating: ", class_name[test])
