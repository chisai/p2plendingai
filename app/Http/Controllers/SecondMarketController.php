<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Auth;
use App\SecondMarket;

class SecondMarketController extends Controller {

    public static function getFillable() { 
        return [
            'LoanId', 'Amount', 'AuctionId', 'LoanPartId','AuctionName', 'AuctionNumber', 'AuctionBidNumber',
            'Country', 'CreditScore', 'Rating', 'Interest', 'UserName', 'LoanStatusActiveFrom', 
            'Gender', 'DateOfBirth', 'SignedDate', 'NextPaymentNr', 'NextPaymentDate', 'NextPaymentSum', 
            'NrOfScheduledPayments', 'OutstandingPayments', 'ListedOnDate'
        ];
    }

    public static function storeItems($amount, $arr) {
  
        foreach ($arr as $arrItems) {
            $secondMarket = new SecondMarket();
            $fillable = SecondMarketController::getFillable();
            foreach ($fillable as $key) {
                if ($key == 'LoanId') {
                    $secondMarket->$key = $arrItems->Id;  
                    continue;
                } 
                if (SecondMarketController::contains(serialize($arrItems->$key), "DateTime")) {
                    $secondMarketTemp = (int) str_replace("DateTime @","", serialize($arrItems->$key));
                    $secondMarketTemp = date("Y-m-d H:i:s",$secondMarketTemp);
                    $secondMarket->$key = $secondMarketTemp;
                } else {
                    $secondMarket->$key = $arrItems->$key;
                }
            }
            try {
                $secondMarket->save();
            } catch (\Exception $e) {
               
            }
        }
        $data = DB::table('secondmarketitem')->orderBy('created_at', 'desc')->limit($amount)->get();
        return $data;
    }

    public static function getItems ($amount) {
        $data = DB::table('secondmarketitem')->orderBy('created_at', 'desc')->limit($amount)->get();
        return $data;
    }
    public static function contains($haystack, $needle, $caseSensitive = false) {
        return $caseSensitive ?
                (strpos($haystack, $needle) === FALSE ? FALSE : TRUE):
                (stripos($haystack, $needle) === FALSE ? FALSE : TRUE);
    }

    public static function madeChoice($id) {
        $data = SecondMarket::find($id);
        if (!$data) {
            return redirect()->route('home');
        }
        $fillable = SecondMarketController::getFillable();
        return view('made-choice', ['data' => $data, 'fillable' => $fillable]);
    }
}