<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Chisai\BondoraHandler;
use App\Http\Controllers\SecondMarketController;
use Cache;
use Session;
use Config;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\SecondMarket;
use App\Transaction;


class bondora extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct () {

    }

    public function authcheck() {
        $b = $this->getHandler();
        $token = Auth::user()->bondora_token;
        if(!$token){
            //dd('token bestaat niet');
            $this->authorizeUser();
        } else {
            $token = $b->setToken($token);
            $user = Auth::user();
            $user->bondora_token = $token;
            $user->save();
        } 
    }

    public function authorizeUser()
    {
        $b = $this->getHandler();
        $b->authorize();
    }

    public function token() {
        $b = $this->getHandler();

        if(request('code')){
            $token = $b->getToken(request('code'));
            $token = $b->setToken($token);
        }
        return redirect('/home');
    }

    public function items() {
        $amount = request('amount');
        $risk = request('risk');
        if (!$risk) {
            $risk = 0;
        } else {
            $risk = 1;
        }
        try {
            $b = $this->getHandler();
            $result = $b->getSecondaryMarketItems($amount,1);
            $data = SecondMarketController::storeItems($amount, $result);
        } catch (\Exception $e) {
            $data = SecondMarketController::getItem($amount);
        }
        $keys = ["id", "LoanId", "AuctionNumber", "Country", "Interest"];
        return view('choices', ['items' => $data, 'keys' => $keys, 'amount' => $amount, 'risk' => $risk]);
    }

    public function loans () {

    }

    public function auction () {

    }

    private function getHandler () {
        if(!Session::has('bon')){
            $config = array(
                'auth' => array(
                    'url_base' => Config::get('services.bondora.baseURL'),
                    'client_id' => Config::get('services.bondora.id'),
                    'secret' => Config::get('services.bondora.secret'),
                    'scope' => Config::get('services.bondora.scope'),
                ),
                'api_base' => Config::get('services.bondora.api'),
            );
            $b = new BondoraHandler($config);
            Session::put('bon', $b);
        }
        $b = Session::get('bon');
        return $b;
    }

    public function placeBid($id) {
        if (!Transaction::where('transaction_id', $id)->first()) {
            $b = $this->getHandler();
            $cost = SecondMarket::where('AuctionId', $id)->first()->Amount;
            $balance = DB::table('history')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
            if (!$balance) {
                Session::put('error', 'You dont have enough money to buy this auction');
                return redirect()->route('home');  
            }
            $balance = $balance->balance;
            if ($balance < $cost) {
                Session::put('error', 'You dont have enough money to buy this auction');
                return redirect()->route('home');
            } else {
                $result = $b->placeBid($id, $cost);
            }
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id; 
            $transaction->transaction_id = $id;
            $transaction->transaction_type = 'investment';
            $transaction->amount = $cost;
            $transaction->balance = $balance - $cost;
            $transaction->save();
            Session::put('status', 'Bid accepted!');
            return redirect()->route('home');
        } else {
            Session::put('error', 'You have already invested in this auction');
            return redirect()->route('home');
        }
    }
}
