<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = DB::table('history')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
        $workingDir = getcwd();
        $temp = strlen($workingDir);
        $workingDir = substr($workingDir, -$temp, -6) . "resources/scripts";
        if (!$data) {
            $balance = 0;
        } else {
            $balance = $data->balance;
        }
        Session::put('workingDir', $workingDir);
        return view('home', ['balance' => $balance]);
    }
}
