<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Image;
use App\User;
use App\Transaction;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Payout;
use Config;

class UserController extends Controller
{
    //
    public function profile() {
		$transactions = Transaction::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
		$balance = UserController::getBalance();
		return view('profile', ['user' => Auth::user(), 'transactions' => $transactions, 'balance' => $balance]);
	}
	
    public function updateAvatar(Request $request){

    	// Handle the user upload of avatar
    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save(public_path().'/uploads/avatars/' . $filename );

    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();
    	}
    	return view('profile', ['user' => Auth::user()]);
    }

	public function editUser(User $user) {

		if (Auth::user()->id != $user->id) {
            if (Auth::user()->role_id != 1) {
                return redirect()->back();    
            }
        }
		
		$this->validate(request() ,[
			'password' => ''
		]);
		
		if (!empty(request('password'))) { 
            $user->password = bcrypt(request('password'));
        } else {
            $user->password = $user->password;
        }
		$user->save();
		return back();
	}

	public function deposit() {
		$balance = UserController::getBalance();
		return view('deposit', ['balance' => $balance]);
	}

	public function withdraw() {
		$balance = UserController::getBalance();
		return view('withdraw', ['balance' => $balance]);
	}

	public function postDeposit() {
		$balance = UserController::getBalance();
		$description = "Charge of: €" . request('amount') . " to Chis-AI";
		Stripe::setApiKey(Config::get('services.stripe.secret'));
        try {
            $charge = Charge::create(array(
                "amount" => floatval(request('amount')) * 100,
                "currency" => 'eur',
                "source" => request('stripeToken'),
                "description" => $description,
                'receipt_email' => Auth::user()->email
			));

			$transaction = new Transaction();
            $transaction->user_id = Auth::user()->id; 
            $transaction->transaction_id = $charge->id;
			$transaction->transaction_type = 'deposit';
			$transaction->amount = floatval(request('amount'));
			$transaction->balance = $balance + floatval(request('amount'));
			$transaction->save(); 
			   
        } catch (\Exception $e) {
			dd($e->getMessage());
            return redirect()->route('deposit')->with('error', $e->getMessage());

        }
        return redirect()->route('deposit');
	}
	
	public function postWithdraw() {
		$balance = (float) UserController::getBalance();
		Stripe::setApiKey(Config::get('services.stripe.secret'));
		$this->validate(request(), [
			'account' => 'required',
			'iban' => 'required',
			'swift' => 'required',
			'amount' => 'required'
		]);
		if ($balance - (float) request('amount') < 0) {
			return view('withdraw', ['error' => 'You dont have sufficient funds for this transaction', 'balance' => $balance]);
		} else if ((float) request('amount') < 25) {
			return view('withdraw', ['error' => 'Minimum withdrawal is €25', 'balance' => $balance]);
		} else {
			/* try {
				$payOut = Payout::create([
				"amount" => (float) request('amount') * 100,
				"currency" => "eur",
			  ]);
			*/
			  $transaction = new Transaction();
			  $transaction->user_id = Auth::user()->id; 
			  $transaction->transaction_id = time();
			  $transaction->transaction_type = 'withdrawal';
			  $transaction->amount = floatval(request('amount'));
			  $transaction->balance = $balance - floatval(request('amount'));
			  $transaction->save(); 
				/*
			} catch (\Exception $e) {
				dd($e->getMessage());
			}
			*/
		}
		return redirect()->route('pre.withdraw');
	}

	public function getBalance() {
		$data = Transaction::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
        
        if (!$data) {
            $balance = 0;
        } else {
            $balance = $data->balance;
		}
		return $balance;
	}
}
