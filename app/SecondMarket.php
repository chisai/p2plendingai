<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class SecondMarket extends Model {

    protected $table = 'secondmarketitem';
    protected $fillable = [
        'LoanId', 'Amount', 'AuctionId', 'LoanPartId','AuctionName', 'AuctionNumber', 'AuctionBidNumber',
        'Country', 'CreditScore', 'Rating', 'Interest', 'UserName', 'LoanStatusActiveFrom', 
        'Gender', 'DateOfBirth', 'SignedDate', 'NextPaymentNr', 'NextPaymentDate', 'NextPaymentSum', 
        'NrOfScheduledPayments', 'OutstandingPayments', 'ListedOnDate'
    ];

}