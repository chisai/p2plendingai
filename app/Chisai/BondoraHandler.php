<?php

namespace App\Chisai;

use Petslane\Bondora;

class BondoraHandler
{
    private $config;
    private $api;

    public function __construct ($config) {
        $this->config = $config;
        $this->api = $this->getAPI();
    }

    public function authorize () {
        $url = $this->api->getAuthUrl();
        header('Location:'. $url);
    }

    private function getAPI () {
        if($this->api == null) {
            $this->api = new Bondora\Api($this->config);
        }
        return $this->api;
    }

    public function getToken ($code) {
        $token = $this->api->getToken($code);
        return $token;
    }    

    public function setToken($tokenObj) {
        $token = $tokenObj;
        $now = time();
        $tokenTime =$token->valid_until;
        $deltaTime = $tokenTime - $now;
        if($deltaTime < 0){
            $token = $this->api->refreshToken($token->refresh_token);
        }
        $this->api->setToken($token->access_token);
        return $token;
    }

    public function getSecondaryMarketItems ($size, $temp) {
        $results = $this->api->secondaryMarket([
            'pageSize' => $size,
            'pageNumber' => 1,
        ]);
        return $results;
    }

    public function getLoanPart () {

    }

    public function placeBid ($id, $cost) {
        
        $bid = new Bondora\Definition\Bid(array(
            'AuctionId' => $id,
            'Amount' => $cost,
            'MinAmount' => 1,
        ));
        return $bid;
    }

    public function cancelBid() {

    }
}