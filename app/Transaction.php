<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Transaction extends Model {

    protected $table = 'history';
    protected $fillable = [
        'user_id', 'transaction_id', 'transaction_type','amount', 'balance'
    ];
    
}