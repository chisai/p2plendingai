<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ai:update {--f} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download and process dataset once a day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if ($this->option('f') || $this->option('force')) {
            Updatedata::forceUpdate();
            return;
        }
        $result = DB::table('datasetlog')->orderBy('id', 'desc')->limit(1)->first();

        if ((!$result) || (time() - $result->created_at) >= 86400) {
            try {
                pcntl_async_signals(true);
                pcntl_signal(SIGINT, [$this, 'shutdown']); 
                pcntl_signal(SIGTERM, [$this, 'shutdown']);
                $errors = [];
                $workingDir = getcwd() . "/resources/scripts/";
                $bar = $this->output->createProgressBar(4);
                $this->info('Updating data for the AI');
                $this->info('All files are placed in: ' . $workingDir, 'v');
                for ($i=0; $i<3; $i++) {
                    if ($i == 0) {
                        $this->info('Removing all previous data from the AI', 'v');
                        $this->info('Downloading dataset from https://www.bondora.com/marketing/media/LoanData.zip and unpacking the file');
                    } elseif ($i == 1) {
                        $result = DB::table('datasetlog')->orderBy('id', 'desc')->limit(1)->first(); 
                        if ($result->status != "OK") {
                            array_push($errors, $result->status);
                            break;
                        } else {
                            $this->info('Dataset downloaded and verified','v');
                            $this->info('Parsing data 1/2');
                            $this->info('Writing data into data.csv and labels0.csv','vv');
                        }
                    } else {
                        $this->info('Parsing data 2/2');
                        $this->info('Writing labels1.csv', 'vv');
                    }
                    $temp = getcwd() . "/resources/scripts/./test" . $i;
                    $bar->advance();
                    $this->info(' ');
                    shell_exec($temp);
                }
                if (count($errors) == 0) {
                    $bar->advance();
                    $this->info(' ');
                    $this->info('Removing obsolete files', 'v');
                    $this->info('Done updating all data!');
                    $this->info(' ');
                } else {
                    $this->info('');
                    foreach ($errors as $error) {
                        $this->error($error);
                    }
                    $this->error('Removing all temporary files');
                    $temp = getcwd() . "/resources/scripts/./error";
                    shell_exec($temp);
                }
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
        } else {
            $timeleft = intval((86400 - (time() - $result->created_at)) / (60 * 60));
            if ($timeleft == 1) {
                $timeleft = $timeleft . " hour";
            } else {
                $timeleft = $timeleft . " hours";
            }
            $this->error('Already updated, try updating once a day! Try again in ' . $timeleft);
        }
    }

    public function shutdown() {
        $result = DB::table('datasetlog')->orderBy('id', 'desc')->limit(1)->first(); 
        $this->info('');
        $this->info('Aborted by user');
        DB::table('datasetlog')->where('id', $result->id)->update(['status' => 'Aborted by User!']);
        $signal = true;
    }

    public function forceUpdate() {
        try {
            pcntl_async_signals(true);
            pcntl_signal(SIGINT, [$this, 'shutdown']); 
            pcntl_signal(SIGTERM, [$this, 'shutdown']);
            $this->error('Forcing dataset updates!');
            $workingDir = getcwd() . "/resources/scripts/";
            $bar = $this->output->createProgressBar(4);
            $this->error('Updating data for the AI');
            $this->error('All files are placed in: ' . $workingDir, 'v');
            for ($i=0; $i<3; $i++) {
                // $this->info($signal);
                // if ($signal) { 
                //     break;
                // }
                if ($i == 0) {
                    $this->error('Removing all previous data from the AI', 'v');
                    $this->error('Downloading dataset from https://www.bondora.com/marketing/media/LoanData.zip and unpacking the file');
                } elseif ($i == 1) {
                    $this->error('Dataset downloaded and verified','v');
                    $this->error('Parsing data 1/2');
                    $this->error('Writing data into data.csv and labels0.csv','vv');
                } else {
                    $this->error('Parsing data 2/2');
                    $this->error('Writing labels1.csv', 'vv');
                }
                $temp = getcwd() . "/resources/scripts/./test" . $i;
                $bar->advance();
                $this->info(' ');
                shell_exec($temp);
            }
            $bar->advance();
            $this->info(' ');
            $this->error('Removing obsolete files', 'v');
            $this->error('Done updating all data!');
            $this->info(' '); 
        } catch (\Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
