#!/usr/bin/env python

from __future__ import absolute_import, division, print_function

import tensorflow as tf
from tensorflow import keras, layers
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense

import pandas as pd
#import matplotlib.pyplot as plt
import numpy as np
from random import randint
import mysql.connector

from settings import settings

mydb = mysql.connector.connect(
host=settings("DB_HOST"),
user=settings("DB_USERNAME"),
passwd=settings("DB_PASSWORD"),
port=settings("DB_PORT"),
database=settings("DB_DATABASE")
)
mycursor = mydb.cursor()

def fetchdata():
 
    sql = "SELECT * FROM secondmarketitem ORDER BY id DESC LIMIT 4" 
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    for rsl in myresult:    
        print(rsl[0], " = result van SMI\n")
        sql1 = "SELECT * FROM predictions WHERE auction_id={}".format(rsl[0])
        mycursor.execute(sql1)
        newresult = mycursor.fetchone()
        if not newresult:
            print("Leeg resultaat\nPredictie Volgt nu\n")
            arr = (rsl[0], rsl[13], rsl[22])
            predict(arr)
            #return rsl[0], rsl[13], rsl[22]
        else:
            print (newresult, " = result van pred\n")
    # id = myresult[0]
    # interest = myresult[13]
    # payments = myresult[22]
    # return id, interest, payments
    return True

def storedata(id, rating):

    sql = "INSERT INTO predictions (auction_id, rating) VALUES (%s, %s) ON DUPLICATE KEY UPDATE rating=%s" 
    val = (id, rating, rating)
    mycursor.execute(sql, val)
    mydb.commit()
    return mycursor.rowcount

class_name = [      #Labels rating from high to low
    'AA',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'HR'
]

def predict(array):
    list_predict = np.array([[array[1], array[2]]])
    #test_loss, test_acc = model.evaluate(data, labels)
    predictions = model.predict(list_predict)
    test = np.argmax(predictions[0])

    test1 = test.item()
    print(pred[test1])
    t13 = storedata(array[0], test1)


def get_csv_data (filename):
    csv = pd.read_csv(filename)
    data = csv.copy()
    data = np.array(data)
    return data

model = tf.keras.Sequential(
    [
        tf.keras.layers.Dense(256,activation='relu',input_shape=(2,)),
        tf.keras.layers.Dense(2,activation='softmax')
    ]
)

model.compile(
    optimizer='adam',
    loss="sparse_categorical_crossentropy",
    metrics=['accuracy']
)
#model.summary()

data = get_csv_data('data.csv')
labels = get_csv_data('labels.csv')

model.fit(data, labels, epochs=10, batch_size=512)
pred = ["Low Risk", "High Risk"]

t12 = fetchdata()
#temp = [t12t12[1],t12[2]]
temp1 = 0